plugins {
    id("com.adarshr.test-logger") version "1.7.0"
    id("java-library")
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

repositories {
    mavenCentral()
    maven {
        name = "aspose"
        url = uri("https://repository.aspose.com/repo/")
    }
}

dependencies {
    implementation("com.aspose:aspose-slides:21.3:jdk16")

    testImplementation("org.apache.commons:commons-lang3:3.11")
    testImplementation("org.junit.jupiter:junit-jupiter:5.4.2")
}

tasks.test {
    useJUnitPlatform()
    testLogging {
        events("passed", "skipped", "failed")
    }
}
