import com.aspose.slides.*;

import java.io.PrintStream;

public class PresentationPrintUtils {
    private boolean printObjectRef = false;

    public PresentationPrintUtils() {
        //
    }

    public PresentationPrintUtils setPrintObjectRef(final boolean value) {
        this.printObjectRef = false;
        return this;
    }

    public void print(
            final PrintStream printStream, final String indent,
            final IParagraphFormatEffectiveData format) {
        printStream.println(indent + "alignment: " +
                format.getAlignment());
        printStream.println(indent + "bullet:" + (printObjectRef
                ? " // " + format.getBullet()
                : ""));
        printStream.println(indent + "  " + "// TODO");
        printStream.println(indent + "defaultPortionFormat:" + (printObjectRef
                ? " // " + format.getDefaultPortionFormat()
                : ""));
        print(printStream, indent + "  ",
                format.getDefaultPortionFormat());
        printStream.println(indent + "defaultTabSize: " +
                format.getDefaultTabSize());
        printStream.println(indent + "depth: " +
                format.getDepth());
        printStream.println(indent + "asianLineBreak: " +
                format.getEastAsianLineBreak());
        printStream.println(indent + "fontAlignment: " +
                format.getFontAlignment());
        printStream.println(indent + "hangingPunctuation: " +
                format.getHangingPunctuation());
        printStream.println(indent + "indent: " +
                format.getIndent());
        printStream.println(indent + "latinLineBreak: " +
                format.getLatinLineBreak());
        printStream.println(indent + "marginLeft: " +
                format.getMarginLeft());
        printStream.println(indent + "marginRight: " +
                format.getMarginRight());
        printStream.println(indent + "rightToLeft: " +
                format.getRightToLeft());
        printStream.println(indent + "spaceAfter: " +
                format.getSpaceAfter());
        printStream.println(indent + "spaceBefore: " +
                format.getSpaceBefore());
        printStream.println(indent + "spaceWithin: " +
                format.getSpaceWithin());
        printStream.println(indent + "tabs:" + (printObjectRef
                ? " // " + format.getTabs()
                : ""));
        printStream.println(indent + "  " + "//TODO");
    }

    public void print(
            final PrintStream printStream, final String indent,
            final IParagraphFormat format) {
        printStream.println(indent + "alignment: " + format.getAlignment());
        printStream.println(indent + "bullet:" + (printObjectRef
                ? " // " + format.getBullet()
                : ""));
        printStream.println(indent + "  " + "// TODO");
        printStream.println(indent + "defaultPortionFormat:");
        print(printStream, indent + "  ", format.getDefaultPortionFormat());
        printStream.println(indent + "defaultTabSize:" + format.getDefaultTabSize());
        printStream.println(indent + "depth: " + format.getDepth());
        printStream.println(indent + "eastAsianLineBreak:" + format.getEastAsianLineBreak());
//        printStream.println(indent + "effective: // " + format.getEffective());
//        print(printStream, indent + "  ", format.getEffective());
        printStream.println(indent + "fontAlignment:" + format.getFontAlignment());
        printStream.println(indent + "hangingPunctuation:" + format.getHangingPunctuation());
        printStream.println(indent + "indent: " + format.getIndent());
        printStream.println(indent + "latinLineBreak: " + format.getLatinLineBreak());
        printStream.println(indent + "marginLeft: " + format.getMarginLeft());
        printStream.println(indent + "marginRight: " + format.getMarginRight());
        printStream.println(indent + "rightToLeft: " + format.getRightToLeft());
        printStream.println(indent + "spacerAfter: " + format.getSpaceAfter());
        printStream.println(indent + "spacerBefore: " + format.getSpaceBefore());
        printStream.println(indent + "spacerWithin: " + format.getSpaceWithin());
        printStream.println(indent + "tabs:" + (printObjectRef
                ? " // " + format.getTabs()
                : ""));
        printStream.println(indent + "  " + "//TODO");
    }

    public void print(
            final PrintStream printStream, final String indent,
            final IFontData fontData) {
        if (null == fontData) {
            printStream.println(indent + "// null");
        } else {
            printStream.println(indent + "fontName: " + fontData.getFontName());
        }
    }

    public void print(
            final PrintStream printStream, final String indent,
            final IPortionFormatEffectiveData format) {
        printStream.println(indent + "bookmarkId: " + format.getBookmarkId());
        printStream.println(indent + "alternativeLanguageId: " + format.getAlternativeLanguageId());
        printStream.println(indent + "languageId: " + format.getLanguageId());
        printStream.println(indent + "hyperlink: " + format.getHyperlinkClick());
        printStream.println(indent + "hyperlinkMouseOver: " + format.getHyperlinkMouseOver());
        printStream.println(indent + "complexScriptFont: " + format.getComplexScriptFont());
        // TODO
    }

    public void print(
            final PrintStream printStream, final String indent,
            final IPortionFormat format) {
        if (null == format) {
            printStream.println(indent + "// null");
        } else {
            printStream.println(indent + "fontBold: " + format.getFontBold());
            printStream.println(indent + "fontItalic: " + format.getFontItalic());
            printStream.println(indent + "fontUnderline: " + format.getFontUnderline());
            printStream.println(indent + "fontHeight: " + format.getFontHeight());
            printStream.println(indent + "latinFont:" + (printObjectRef
                    ? " // " + format.getLatinFont()
                    : ""));
            print(printStream, indent + "  ", format.getLatinFont());
            printStream.println(indent + "bookmarkId: " + format.getBookmarkId());
            printStream.println(indent + "alternativeLanguageId: " +
                    format.getAlternativeLanguageId());
            printStream.println(indent + "languageId: " + format.getLanguageId());
//        printStream.println(indent + "effective:");
//        print(printStream, indent + "  ", format.getEffective());
        }
    }

    public void print(
            final PrintStream printStream, final String indent,
            final IPortion portion) {
        printStream.println(indent + "text: " + portion.getText());
        printStream.println(indent + "portionFormat:");
        print(printStream, indent + "  ", portion.getPortionFormat());
//        printStream.println(indent + "coordinates: " +
//                portion.getCoordinates());
        printStream.println(indent + "field: // " + portion.getField());
//        printStream.print(indent + "react: " + portion.getRect());
    }

    public void print(
            final PrintStream printStream, final String indent,
            final IParagraph paragraph) {
        printStream.println(indent + "text: " + paragraph.getText());
        printStream.println(indent + "paragraphFormat:" + (printObjectRef
                ? " // " + paragraph.getParagraphFormat()
                : ""));
        print(printStream, indent + "  ",
                paragraph.getParagraphFormat());
        printStream.println(indent + "endParagraphPortionFormat:" + (printObjectRef
                ? " // " + paragraph.getEndParagraphPortionFormat()
                : ""));
        print(printStream, indent + "  ",
                paragraph.getEndParagraphPortionFormat());
        int i = 0;
        for (final var porition : paragraph.getPortions()) {
            printStream.println(indent + "portion[" + i + "]:" + (printObjectRef
                    ? " // " + porition
                    : ""));
            print(printStream, indent + "  ", porition);
            ++i;
        }
    }

    public void print(
            final PrintStream printStream, final String indent,
            final IHyperlinkQueries hyperlinkQueries) {
        printStream.println(indent + "anyHyperlinks:" + (printObjectRef
                ? " // " + hyperlinkQueries.getAnyHyperlinks()
                : ""));
        printStream.println(indent + "// TODO size " +
                hyperlinkQueries.getAnyHyperlinks().size());
        printStream.println(indent + "hyperlinkClicks:" + (printObjectRef
                ? " // " + hyperlinkQueries.getHyperlinkClicks()
                : ""));
        printStream.println(indent + "// TODO size " +
                hyperlinkQueries.getHyperlinkClicks().size());
        printStream.println(indent + "hyperlinkMouseOvers:" + (printObjectRef
                ? " // " + hyperlinkQueries.getHyperlinkMouseOvers()
                : ""));
        printStream.println(indent + "// TODO size " +
                hyperlinkQueries.getHyperlinkMouseOvers().size());
    }

    public void print(
            final PrintStream printStream, final String indent,
            final ITextFrameFormatEffectiveData data) {
        printStream.println(indent + "anchoringType: " +
                data.getAnchoringType());
        printStream.println(indent + "autofitType: " +
                data.getAutofitType());
        printStream.println(indent + "centerText: " +
                data.getCenterText());
        printStream.println(indent + "marginBottom: " +
                data.getMarginBottom());
        printStream.println(indent + "marginLeft: " +
                data.getMarginLeft());
        printStream.println(indent + "marginRight: " +
                data.getMarginRight());
        printStream.println(indent + "marginTop: " +
                data.getMarginTop());
        printStream.println(indent + "textStyle:" + (printObjectRef
                ? " // " + data.getTextStyle()
                : ""));
        print(printStream, indent + "  ", data.getTextStyle());
        printStream.println(indent + "textVerticalType: " +
                data.getTextVerticalType());
        printStream.println(indent + "wrapText: " +
                data.getWrapText());
    }

    public void print(
            final PrintStream printStream, final String indent,
            final ITextStyleEffectiveData data) {
        printStream.println(indent + "defaultParagraphFormat:" + (printObjectRef
                ? " // " + data.getDefaultParagraphFormat()
                : ""));
        print(printStream, indent + "  ",
                data.getDefaultParagraphFormat());
    }

    public void print(
            final PrintStream printStream, final String indent,
            final ITextStyle style) {
        printStream.println(indent + "defaultParagraphFormat:" + (printObjectRef
                ? " // " + style.getDefaultParagraphFormat()
                : ""));
        print(printStream, indent + "  ",
                style.getDefaultParagraphFormat());
        printStream.println(indent + "effective:" + (printObjectRef
                ? " // " + style.getEffective()
                : ""));
        print(printStream, indent + "  ", style.getEffective());
    }

    public void print(
            final PrintStream printStream, final String indent,
            final ITextFrameFormat format) {
        printStream.println(indent + "anchoringType: " +
                format.getAnchoringType());
        printStream.println(indent + "autofitType: " +
                format.getAutofitType());
        printStream.println(indent + "centerText: " +
                format.getCenterText());
        printStream.println(indent + "columnCount: " +
                format.getColumnCount());
        printStream.println(indent + "columnSpacing: " +
                format.getColumnSpacing());
//        printStream.println(indent + "effective: // " +
//                format.getEffective());
//        print(printStream, indent + "  ", format.getEffective());
        printStream.println(indent + "marginBottom: " +
                format.getMarginBottom());
        printStream.println(indent + "marginLeft: " +
                format.getMarginLeft());
        printStream.println(indent + "marginRight: " +
                format.getMarginRight());
        printStream.println(indent + "marginTop: " +
                format.getMarginTop());
        printStream.println(indent + "rotationAngle: " +
                format.getRotationAngle());
        printStream.println(indent + "textStyle:" + (printObjectRef
                ? " // " + format.getTextStyle()
                : ""));
        print(printStream, indent + "  ", format.getTextStyle());
        printStream.println(indent + "textVerticalType: " +
                format.getTextVerticalType());
        printStream.println(indent + "wrapText: " +
                format.getWrapText());
    }

    public void print(
            final PrintStream printStream, final String indent,
            final ITextFrame textFrame) {
        int i = 0;
        for (final var paragraph : textFrame.getParagraphs()) {
            printStream.println(indent + "paragraph[" + i + "]:" + (printObjectRef
                    ? " // " + paragraph
                    : ""));
            print(printStream, indent + "  ", paragraph);
            ++i;
        }
        printStream.println(indent + "text: " + textFrame.getText());
        printStream.println(indent + "hyperlinkQueries:" + (printObjectRef
                ? " // " + textFrame.getHyperlinkQueries()
                : ""));
        print(printStream, indent + "  ", textFrame.getHyperlinkQueries());
        printStream.println(indent + "textFrameFormat:" + (printObjectRef
                ? " // " + textFrame.getTextFrameFormat()
                : ""));
        print(printStream, indent + "  ",
                textFrame.getTextFrameFormat());
    }

    public void print(final PrintStream printStream,
                      final String indent, final IChartParagraphFormat format) {
        printStream.println(indent + "fontAlignment: " +
                format.getFontAlignment());
        printStream.println(indent + "alignment: " +
                format.getAlignment());
        printStream.println(indent + "defaultTabSize: " +
                format.getDefaultTabSize());
        printStream.println(indent + "eastAsianLineBreak: " +
                format.getEastAsianLineBreak());
        printStream.println(indent + "hangingPunctuation: " +
                format.getHangingPunctuation());
        printStream.println(indent + "indent: " +
                format.getIndent());
        printStream.println(indent + "latinLineBreak: " +
                format.getLatinLineBreak());
        printStream.println(indent + "marginLeft: " +
                format.getMarginLeft());
        printStream.println(indent + "marginRight: " +
                format.getMarginRight());
        printStream.println(indent + "rightToLeft: " +
                format.getRightToLeft());
        printStream.println(indent + "spacerAfter: " +
                format.getSpaceAfter());
        printStream.println(indent + "spacerBefore: " +
                format.getSpaceBefore());
        printStream.println(indent + "spacerWithin: " +
                format.getSpaceWithin());
        printStream.println(indent + "tabs:" + (printObjectRef
                ? " // " + format.getTabs()
                : ""));
        printStream.println(indent + "  " + "//TODO");
    }

    public void print(final PrintStream printStream,
                      final String indent, final IChartPortionFormat format) {
        printStream.println(indent + "fontAlignment: " +
                format.getLanguageId());
        printStream.println(indent + "alternativeLanguageId: " +
                format.getAlternativeLanguageId());
        printStream.println(indent + "complexScriptFont:" + (printObjectRef
                ? " // " + format.getComplexScriptFont()
                : ""));
        print(printStream, indent + "  ", format.getComplexScriptFont());
        printStream.println(indent + "eastAsianFont:" + (printObjectRef
                ? " // " + format.getEastAsianFont()
                : ""));
        print(printStream, indent + "  ", format.getEastAsianFont());
        printStream.println(indent + "effectFormat:" + (printObjectRef
                ? " // " + format.getEffectFormat()
                : ""));
        printStream.println(indent + "escapement:" + (printObjectRef
                ? " // " + format.getEscapement()
                : ""));
        printStream.println(indent + "fillFormat:" + (printObjectRef
                ? " // " + format.getFillFormat()
                : ""));
        printStream.println(indent + "fontBold: " + format.getFontBold());
        printStream.println(indent + "fontHeight: " + format.getFontHeight());
        printStream.println(indent + "fontItalic: " + format.getFontItalic());
        printStream.println(indent + "fontUnderline: " +
                format.getFontUnderline());
        printStream.println(indent + "highlightColor:" + (printObjectRef
                ? " // " + format.getHighlightColor()
                : ""));
        printStream.println(indent + "kerningMinimalSize: " +
                format.getKerningMinimalSize());
        printStream.println(indent + "kumimoji: " +
                format.getKumimoji());
        printStream.println(indent + "latinFont:" + (printObjectRef
                ? " // " + format.getLatinFont()
                : ""));
        print(printStream, indent + "  ", format.getLatinFont());
        printStream.println(indent + "lineFormat:" + (printObjectRef
                ? " // " + format.getLineFormat()
                : ""));
        printStream.println(indent + "normiliseHeight: " +
                format.getNormaliseHeight());
        printStream.println(indent + "proofDisabled: " +
                format.getProofDisabled());
        printStream.println(indent + "spacing: " +
                format.getSpacing());
        printStream.println(indent + "strikethroughType: " +
                format.getStrikethroughType());
        printStream.println(indent + "symbolFont:" + (printObjectRef
                ? " // " + format.getSymbolFont()
                : ""));
        print(printStream, indent + "  ", format.getSymbolFont());
        printStream.println(indent + "textCapType: " +
                format.getTextCapType());
        printStream.println(indent + "underlineFillFormat:" + (printObjectRef
                ? " // " + format.getUnderlineFillFormat()
                : ""));
        printStream.println(indent + "underlineLineFormat:" + (printObjectRef
                ? " // " + format.getUnderlineLineFormat()
                : ""));
    }

    public void print(final PrintStream printStream,
                      final String indent, final IChartTextBlockFormat format) {
        printStream.println(indent + "fontAlignment: " + format.getAnchoringType());
    }

    public void print(final PrintStream printStream,
                      final String indent, final IChartTextFormat textFormat) {
        printStream.println(indent + "paragraphFormat:" + (printObjectRef
                ? " // " + textFormat.getParagraphFormat()
                : ""));
        print(printStream, indent + "  ",
                textFormat.getParagraphFormat());
        printStream.println(indent + "portionFormat:" + (printObjectRef
                ? " // " + textFormat.getPortionFormat()
                : ""));
        print(printStream, indent + "  ",
                textFormat.getPortionFormat());
        printStream.println(indent + "textBlockFormat:" + (printObjectRef
                ? " // " + textFormat.getTextBlockFormat()
                : ""));
        print(printStream, indent + "  ",
                textFormat.getTextBlockFormat());
    }

    public void print(final PrintStream printStream,
                      final String indent, final IDataLabelFormat format) {
        printStream.println(indent + "showValue: " + format.getShowValue());
        printStream.println(indent + "position: " + format.getPosition());
        printStream.println(indent + "separator: " + format.getSeparator());
        printStream.println(indent + "showSeriesName: " + format.getShowSeriesName());
        printStream.println(indent + "showCategoryName: " + format.getShowCategoryName());
        printStream.println(indent + "showLegendKey: " + format.getShowLegendKey());
        printStream.println(indent + "showPercentage: " + format.getShowPercentage());
        printStream.println(indent + "showLeaderLines: " + format.getShowLeaderLines());
        printStream.println(indent + "showLabelAsDataCallout: " + format.getShowLabelAsDataCallout());
        printStream.println(indent + "showLabelValueFromCell: " + format.getShowLabelValueFromCell());
        printStream.println(indent + "numberFormat: " + format.getNumberFormat());
        printStream.println(indent + "textFormat:" + (printObjectRef
                ? " // " + format.getTextFormat()
                : ""));
        print(printStream, indent + "  ", format.getTextFormat());
    }

    public void print(final PrintStream printStream,
                      final String indent, final IChartDataWorksheet sheet) {
        printStream.println(indent + "index: " + sheet.getIndex());
    }

    public void print(final PrintStream printStream,
                      final String indent, final IChartDataCell cell) {
        if (null == cell) {
            printStream.println(indent + "// null");
        } else {
            printStream.println(indent + "customNumberFormat:" +
                    cell.getCustomNumberFormat());
            printStream.println(indent + "chartDataWorksheet:" + (printObjectRef
                    ? " // " + cell.getChartDataWorksheet()
                    : ""));
            print(printStream, indent + "  ", cell.getChartDataWorksheet());
            printStream.println(indent + "column: " +
                    cell.getColumn());
            printStream.println(indent + "formula: " +
                    cell.getFormula());
            printStream.println(indent + "presetNumberFormat: " +
                    cell.getPresetNumberFormat());
            printStream.println(indent + "r1C1Formula: " +
                    cell.getR1C1Formula());
            printStream.println(indent + "row: " +
                    cell.getRow());
            printStream.println(indent + "value: " +
                    cell.getValue());
        }
    }

    public void print(final PrintStream printStream,
                      final String indent, final IDataLabel label) {
        printStream.println(indent + "actualText: " +
                label.getActualLabelText());
        printStream.println(indent + "textFrameForOverriding:" + (printObjectRef
                ? " // " + label.getTextFrameForOverriding()
                : ""));
        print(printStream, indent + "  ",
                label.getTextFrameForOverriding());
        printStream.println(indent + "dataLabelFormat:" + (printObjectRef
                ? " // " + label.getDataLabelFormat()
                : ""));
        print(printStream, indent + "  ", label.getDataLabelFormat());
        printStream.println(indent + "valueFromCell:" + (printObjectRef
                ? " // " + label.getValueFromCell()
                : ""));
        print(printStream, indent + "  ", label.getValueFromCell());
        printStream.println(indent + "actualHeight: " +
                label.getActualHeight());
        printStream.println(indent + "actualWidth: " +
                label.getActualWidth());
        printStream.println(indent + "actualX: " +
                label.getActualX());
        printStream.println(indent + "actualY: " +
                label.getActualY());
        printStream.println(indent + "bottom: " +
                label.getBottom());
        printStream.println(indent + "height: " +
                label.getHeight());
        printStream.println(indent + "right: " +
                label.getRight());
        printStream.println(indent + "textFormat:" + (printObjectRef
                ? " // " + label.getTextFormat()
                : ""));
        print(printStream, indent + "  ", label.getTextFormat());
        printStream.println(indent + "width: " +
                label.getWidth());
        printStream.println(indent + "x: " +
                label.getX());
        printStream.println(indent + "y: " +
                label.getY());
    }

    public void print(final PrintStream printStream,
                      final String indent, final IChartDataPoint dataPoint) {
        printStream.println(indent + "label:" + (printObjectRef
                ? " // " + dataPoint.getLabel()
                : ""));
        print(printStream, indent + "  ", dataPoint.getLabel());
    }

    public void print(final PrintStream printStream,
                      final String indent, final IChartSeries chartSeries) {
        int i = 0;
        for (final var dataPoint : chartSeries.getDataPoints()) {
            printStream.println(indent + "dataPoint[" + i + "]:" + (printObjectRef
                    ? " // " + dataPoint
                    : ""));
            print(printStream, indent + "  ", dataPoint);
            ++i;
        }
        int j = 0;
        for (final var label : chartSeries.getLabels()) {
            printStream.println(indent + "label[" + j + "]:" + (printObjectRef
                    ? " // " + label
                    : ""));
            print(printStream, indent + "  ", label);
            ++j;
        }
    }

    public void print(final PrintStream printStream,
                      final String indent, final IChartCategory category) {
        printStream.println(indent + "useCell: " + category.getUseCell());
        if (category.getUseCell()) {
            printStream.println(indent + "asCell:" + (printObjectRef
                    ? " // " + category.getAsCell()
                    : ""));
            print(printStream, indent + "  ", category.getAsCell());
        } else {
            printStream.println(indent + "asLiteral:" + category.getAsLiteral());
        }
        printStream.println(indent + "value: " + category.getValue());
    }

    public void print(final PrintStream printStream,
                      final String indent, final IChartDataWorkbook workbook) {
        printStream.println(indent + "// TODO");
    }

    public void print(final PrintStream printStream,
                      final String indent, final IChartData chartData) {
        int i = 0;
        for (final var serie : chartData.getSeries()) {
            printStream.println(indent + "serie[" + i + "]:" + (printObjectRef
                    ? " // " + serie
                    : ""));
            print(printStream, indent + "  ", serie);
            ++i;
        }
        int j = 0;
        for (final var category : chartData.getCategories()) {
            printStream.println(indent + "category[" + j + "]:" + (printObjectRef
                    ? " // " + category
                    : ""));
            print(printStream, indent + "  ", category);
            ++j;
        }
        printStream.println(indent + "chartDataWorkbook:" + (printObjectRef
                ? " // " + chartData.getChartDataWorkbook()
                : ""));
        print(printStream, indent + "  ", chartData.getChartDataWorkbook());
        printStream.println(indent + "dataSourceType: " +
                chartData.getDataSourceType());
    }

    public void print(final PrintStream printStream,
                      final String indent, final IDataTable dataTable) {
        printStream.println(indent + "format: " + (printObjectRef
                ? " // " + dataTable.getFormat()
                : ""));
        printStream.println(indent + "// TODO");
        printStream.println(indent + "showLegendKey: " +
                dataTable.getShowLegendKey());
        printStream.println(indent + "textFormat:" + (printObjectRef
                ? " // " + dataTable.getTextFormat()
                : ""));
        print(printStream, indent + "  ", dataTable.getTextFormat());
    }

    public void print(final PrintStream printStream,
                      final String indent, final Chart chart) {
        printStream.println(indent + "chartData:" + (printObjectRef
                ? " // " + chart.getChartData()
                : ""));
        print(printStream, indent + "  ", chart.getChartData());
        printStream.println(indent + "chartDataTable:" + (printObjectRef
                ? " // " + chart.getChartDataTable()
                : ""));
        print(printStream, indent + "  ", chart.getChartDataTable());
        printStream.println(indent + "textFormat:" + (printObjectRef
                ? " // " + chart.getTextFormat()
                : ""));
        print(printStream, indent + "  ", chart.getTextFormat());
    }
}
