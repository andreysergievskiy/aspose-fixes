import com.aspose.slides.Presentation;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

public class Aspose20200821PptxLoadingTest {

    /**
     * Aspose fails to load file. LibreOffice and Microsoft native products
     * have no problem (even warnings) on it.
     *
     * File type is detected as "Microsoft PowerPoint 2007+" (in Linux),
     * same as other successfully loaded files.
     *
     * Thrown exception is:
     * class com.aspose.slides.PptxReadException: Specified argument was out of the range of valid values.
     *   Parameter name: Parameter name: index ---> class com.aspose.slides.exceptions.ArgumentOutOfRangeException: Specified argument was out of the range of valid values.
     *   Parameter name: Parameter name: index
     *   com.aspose.slides.Collections.Generic.List.get_Item(Unknown Source)
     */
    @Test
    public void testPptxLoading() {
        assertDoesNotThrow(() -> {
            try (final var inputStream =
                         getClass().getResourceAsStream(
                                 "Aspose20200821PptxLoadingTest.pptx")) {
                assertDoesNotThrow(() -> new Presentation(inputStream));
            }
        });
    }
}
