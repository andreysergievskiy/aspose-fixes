import com.aspose.slides.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class Aspose20200827EditDataLabelsTest {

    @BeforeAll
    public static void beforeAll() {
        new com.aspose.slides.License().setLicense(
                Aspose20200827EditDataLabelsTest.class
                        .getResourceAsStream("/Aspose.Slides.lic"));
    }

    private Collection<Chart> findCharts(final Presentation presentation) {
        final var result = new ArrayList<Chart>();
        for (final var slide : presentation.getSlides()) {
            for (final var shape : slide.getShapes()) {
                if (shape instanceof Chart) {
                    result.add((Chart) shape);
                }
            }
        }
        return result;
    }

    @Test
    public void testPptxLoadSave() {
        final var inputFilename = getClass().getSimpleName() + ".pptx";
        assertDoesNotThrow(() -> {
            try (final var inputStream =
                         getClass().getResourceAsStream(inputFilename)) {
                final var presentation = new Presentation(inputStream);
                final var charts = findCharts(presentation);
                int i = 0;
                for (final var chart : charts) {
                    if (ChartType.Funnel == chart.getType()) {
                        System.out.println("chart[" + i + "]:");
                        new PresentationPrintUtils()
                                .print(System.out, "  ", chart);
                    }
                    ++i;
                }
                final var outputFilename = getClass().getSimpleName() +
                        ".testPptxLoadSave.pptx";
                try (final var os = new FileOutputStream(outputFilename)) {
                    presentation.save(os, SaveFormat.Pptx);
                }
            }
        });
    }

    private static class DataItem {
        private String name = "";
        private double value = 0.0d;
        private String format = "0.00";
        private Color dataColor = Color.RED;
        private Color labelBgColor = Color.GREEN;
        private Color labelFgColor = Color.BLUE;

        public String getName() {
            return name;
        }

        public DataItem setName(final String name) {
            this.name = name;
            return this;
        }

        public double getValue() {
            return value;
        }

        public DataItem setValue(final double value) {
            this.value = value;
            return this;
        }

        public String getFormat() {
            return format;
        }

        public DataItem setFormat(final String format) {
            this.format = format;
            return this;
        }

        public Color getDataColor() {
            return dataColor;
        }

        public DataItem setDataColor(final Color dataColor) {
            this.dataColor = dataColor;
            return this;
        }

        public Color getLabelBgColor() {
            return labelBgColor;
        }

        public DataItem setLabelBgColor(final Color labelBgColor) {
            this.labelBgColor = labelBgColor;
            return this;
        }

        public Color getLabelFgColor() {
            return labelFgColor;
        }

        public DataItem setLabelFgColor(final Color labelFgColor) {
            this.labelFgColor = labelFgColor;
            return this;
        }
    }

    private final List<DataItem> items = List.of(
            new DataItem()
                    .setName("R1")
                    .setValue(25.22324576d)
                    .setFormat("↑ 0.000 x")
                    .setDataColor(Color.RED.darker())
                    .setLabelBgColor(Color.YELLOW)
                    .setLabelFgColor(Color.RED),
            new DataItem()
                    .setName("R2")
                    .setValue(15.22324576d)
//                    .setFormat("↓ 0.000 x")
                    .setFormat(" ")
                    .setDataColor(Color.BLUE.darker())
//                    .setLabelBgColor(Color.CYAN)
                    .setLabelBgColor(null)
//                    .setLabelFgColor(Color.BLUE)
                    .setLabelFgColor(null)
    );

    private Presentation createPresentation() {
        final var presentation = new Presentation();
        final var slideSize = presentation.getSlideSize();
        slideSize.setSize(SlideSizeType.OnScreen16x9, SlideSizeScaleType.Maximize);
        slideSize.setOrientation(SlideOrienation.Landscape);
        return presentation;
    }

    private void saveToFile(final Presentation presentation,
                            final int saveFormat, // from com.aspose.slides.SaveFormat
                            final String filename) {
        try (final var fos = new FileOutputStream(filename)) {
            presentation.save(fos, saveFormat);
        } catch (final IOException ioe) {
            System.out.println(ioe.getMessage());
            throw new RuntimeException(ioe);
        }
    }

    private interface ChartTypeStrategy {
        IChart addChart(IShapeCollection shapes,
                        float x, float y, float width, float height);

        IChartSeries addSerie(IChartSeriesCollection series);

        IChartDataPoint addDataPoint(IChartDataPointCollection dataPoints,
                                     IChartDataCell dataCell);
    }

    private static class ChartTypeStrategyFunnel implements ChartTypeStrategy {
        @Override
        public IChart addChart(final IShapeCollection shapes,
                               final float x, final float y,
                               final float width, final float height) {
            return shapes.addChart(ChartType.Funnel, x, y, width, height);
        }

        @Override
        public IChartSeries addSerie(final IChartSeriesCollection series) {
            return series.add(ChartType.Funnel);
        }

        @Override
        public IChartDataPoint addDataPoint(
                final IChartDataPointCollection dataPoints,
                final IChartDataCell dataCell) {
            return dataPoints.addDataPointForFunnelSeries(dataCell);
        }
    }

    private static class ChartTypeStrategyColumn implements ChartTypeStrategy {
        @Override
        public IChart addChart(final IShapeCollection shapes,
                               final float x, final float y,
                               final float width, final float height) {
            return shapes.addChart(ChartType.ClusteredColumn, x, y, width, height);
        }

        @Override
        public IChartSeries addSerie(final IChartSeriesCollection series) {
            return series.add(ChartType.ClusteredColumn);
        }

        @Override
        public IChartDataPoint addDataPoint(
                final IChartDataPointCollection dataPoints,
                final IChartDataCell dataCell) {
            return dataPoints.addDataPointForBarSeries(dataCell);
        }
    }

    public Presentation renderChart(
            final ChartTypeStrategy chartTypeStrategy,
            final float width, final float height) {
        final var presentation = createPresentation();

        final var slides = presentation.getSlides();
        final var slide = slides.get_Item(0);
        final var shapes = slide.getShapes();
        final var chart = chartTypeStrategy.addChart(shapes,
                20.0f, 20.0f, width, height);

        chart.getAxes().getVerticalAxis().setVisible(false);
        final var data = chart.getChartData();

        final var workBook = data.getChartDataWorkbook();
        workBook.clear(0);

        final var categories = data.getCategories();
        categories.clear();

        final var series = data.getSeries();
        series.clear();

        final var serie = chartTypeStrategy.addSerie(series);
        final var serieLabels = serie.getLabels();
        final var defaultDataLabelFormat =
                serieLabels.getDefaultDataLabelFormat();
        defaultDataLabelFormat.setShowValue(true);
        defaultDataLabelFormat.setShowLabelValueFromCell(false);
        defaultDataLabelFormat.setNumberFormatLinkedToSource(false);
        defaultDataLabelFormat.getTextFormat()
                .getPortionFormat().setFontHeight(24.f);

        final var dataPoints = serie.getDataPoints();

        int i = 0;
        for (final var item : items) {
            final var name = item.getName();
            final var workbookCell =
                    workBook.getCell(0, i, 0, name);

            final var category = categories.add(workbookCell);

            final var value = item.getValue();

            final var dataCell =
                    workBook.getCell(0, i, 1, value);
            dataCell.setCustomNumberFormat(item.getFormat());

            final var dataPoint =
                    chartTypeStrategy.addDataPoint(dataPoints, dataCell);

            final var dataPointFill =
                    dataPoint.getFormat().getFill();
            dataPointFill.setFillType(FillType.Solid);
            dataPointFill.getSolidFillColor().setColor(item.getDataColor());

            final var label = dataPoint.getLabel();

            final var dataLabelFormat =
                    label.getDataLabelFormat();
            assertNotSame(dataLabelFormat, defaultDataLabelFormat);

            dataLabelFormat.setShowValue(true);
            dataLabelFormat.setShowLabelValueFromCell(false);
            dataLabelFormat.setNumberFormatLinkedToSource(false);
            final var dataLabelTextFormat =
                    dataLabelFormat.getTextFormat();
            if (ChartType.Funnel != chart.getType()) {
                dataLabelTextFormat.getPortionFormat().setFontHeight(24.f);
            } else {
                dataLabelTextFormat.getPortionFormat().setFontHeight(Float.NaN);
            }
            dataLabelFormat.setNumberFormat(item.getFormat());
            final var chartValue = dataPoint.getValue();
            if (DataSourceType.Worksheet == chartValue.getDataSourceType()) {
                Assertions.assertTrue(dataCell == chartValue.getAsCell());
//                chartValue.getAsCell().setCustomNumberFormat(item.getFormat());
            }

            label.getTextFrameForOverriding().joinPortionsWithSameFormatting();
            label.getTextFrameForOverriding().getParagraphs().forEach(ph -> {
                ph.getPortions().forEach(pn -> {
                    pn.getPortionFormat().setFontHeight(Float.NaN);
                });
            });

            final var bgFillFormat =
                    dataLabelFormat.getFormat().getFill();
            bgFillFormat.setFillType(FillType.Solid);
            bgFillFormat.getSolidFillColor().setColor(item.getLabelBgColor());
            final var textFormat =
                    dataLabelFormat.getTextFormat();
            final var fgFillFormat =
                    textFormat.getPortionFormat().getFillFormat();
            fgFillFormat.setFillType(FillType.Solid);
            fgFillFormat.getSolidFillColor().setColor(item.getLabelFgColor());
//            final var labelTextFormat = label.getTextFormat();
//            assertNotSame(labelTextFormat, dataLabelTextFormat);
//            labelTextFormat.getPortionFormat().setFontHeight(Float.NaN);

            ++i;
        }

        chart.setTitle(false);
        chart.setLegend(false);

        Optional.ofNullable(chart.getAxes().getHorizontalAxis())
                .ifPresent(axis -> {
                    axis.setVisible(true);
                    axis.setNumberFormat("0.00");
                    final var textFormat = axis.getTextFormat();
                    textFormat.getPortionFormat().setFontHeight(24.f);
//                    axis.setNumberFormatLinkedToSource(false);
                });
        Optional.ofNullable(chart.getAxes().getVerticalAxis())
                .ifPresent(axis -> {
                    axis.setVisible(true);
                    axis.setNumberFormat("0.00");
                    final var textFormat = axis.getTextFormat();
                    textFormat.getPortionFormat().setFontHeight(24.f);
//                    axis.setNumberFormatLinkedToSource(false);
                });

        return presentation;
    }

    @Test
    public void testPptxNewFunnelChart() {
        final var outputFilename = getClass().getSimpleName() +
                ".testPptxNewFunnelChart.pptx";
        final var presentation = renderChart(
                new ChartTypeStrategyFunnel(), 600, 300);
        saveToFile(presentation, SaveFormat.Pptx, outputFilename);
    }

    @Test
    public void testPptxNewColumnChart() {
        final var outputFilename = getClass().getSimpleName() +
                ".testPptxNewColumnChart.pptx";
        final var presentation = renderChart(
                new ChartTypeStrategyColumn(), 600, 300);
        saveToFile(presentation, SaveFormat.Pptx, outputFilename);
    }
}
