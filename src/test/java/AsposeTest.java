import com.aspose.slides.*;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.io.FileOutputStream;
import java.io.IOException;

public class AsposeTest {

    private static final int N = 2;
    private static final String NAMES[] = {"R1", "R2"};
    private static final double VALUES[] = {25.22324576d, 15.22324576d};
    private static final Color COLORS[] = {Color.GREEN, Color.BLUE};

    private Presentation createPresentation() {
        final var presentation = new Presentation();
        final var slideSize = presentation.getSlideSize();
        slideSize.setSize(SlideSizeType.OnScreen16x9, SlideSizeScaleType.Maximize);
        slideSize.setOrientation(SlideOrienation.Landscape);
        return presentation;
    }

    private void saveToFile(final Presentation presentation,
                            final int saveFormat, // from com.aspose.slides.SaveFormat
                            final String filename) {
        try (final var fos = new FileOutputStream(filename)) {
            presentation.save(fos, saveFormat);
        } catch (final IOException ioe) {
            System.out.println(ioe.getMessage());
            throw new RuntimeException(ioe);
        }
    }

    public Presentation renderPyramid(final float width, final float height) {
        final var presentation = createPresentation();

        final var slides = presentation.getSlides();
        final var slide = slides.get_Item(0);
        final var shapes = slide.getShapes();
        final var chart = shapes.addChart(ChartType.Funnel,
                20.0f, 20.0f, width, height);

        chart.getAxes().getVerticalAxis().setVisible(false);
        final var data = chart.getChartData();

        final var workBook = data.getChartDataWorkbook();
        workBook.clear(0);

        final var categories = data.getCategories();
        categories.clear();

        final var series = data.getSeries();
        series.clear();

        final var serie = series.add(ChartType.Funnel);
        final var dataPoints = serie.getDataPoints();

        for (int i = 0; i < N; ++i) {
            final var name = NAMES[i];
            final var workbookCell = workBook.getCell(0, i, 0, name);

            final var category = categories.add(workbookCell);
        }

        for (int i = 0; i < N; ++i) {
            final var value = VALUES[i];

            final var dataCell = workBook.getCell(0, i, 1, value);
            dataCell.setCustomNumberFormat("0.00");

            final var dataPoint = dataPoints.addDataPointForFunnelSeries(dataCell);
            final var dataPointFill = dataPoint.getFormat().getFill();
            dataPointFill.setFillType(FillType.Solid);

            final var color = COLORS[i];
            dataPointFill.getSolidFillColor().setColor(color);
        }

        chart.setTitle(false);
        chart.setLegend(false);

        return presentation;
    }

    public Presentation renderPie(final float width, final float height) {
        final var presentation = createPresentation();

        final var slides = presentation.getSlides();
        final var slide = slides.get_Item(0);
        final var shapes = slide.getShapes();
        final var chart = shapes.addChart(ChartType.Pie,
                20.0f, 20.0f, width, height);

        final var data = chart.getChartData();

        final var workBook = data.getChartDataWorkbook();
        workBook.clear(0);

        final var categories = data.getCategories();
        categories.clear();

        final var series = data.getSeries();
        series.clear();

        final var serie = series.add(ChartType.Pie);
        final var dataPoints = serie.getDataPoints();

        for (int i = 0; i < N; ++i) {
            final var name = NAMES[i];
            final var workbookCell = workBook.getCell(0, i, 0, name);

            final var category = categories.add(workbookCell);
        }

        for (int i = 0; i < N; ++i) {
            final var value = VALUES[i];

            final var dataCell = workBook.getCell(0, i, 1, value);
            dataCell.setCustomNumberFormat("0.00");

            final var dataPoint = dataPoints.addDataPointForPieSeries(dataCell);
            final var dataPointFill = dataPoint.getFormat().getFill();
            dataPointFill.setFillType(FillType.Solid);

            final var color = COLORS[i];
            dataPointFill.getSolidFillColor().setColor(color);
        }

        chart.setTitle(false);
        chart.setLegend(false);

        return presentation;
    }

    private Presentation renderPyramidWithLabels(final float width,
                                                 final float height) {
        final var presentation = renderPyramid(width, height);
        presentation.getSlides().forEach(slide -> {
            slide.getShapes().forEach(shape -> {
                if (shape instanceof IChart) {
                    final var chart = (IChart) shape;
                    final var chartData = chart.getChartData();
                    chartData.getSeries().forEach(serie -> {
                        serie.getDataPoints().forEach(dataPoint -> {
                            final var label = dataPoint.getLabel();
                            label.getDataLabelFormat().setShowValue(true);
                        });
                    });
                }
            });
        });
        return presentation;
    }

    private Presentation renderPieWithLabels(final float width,
                                             final float height) {
        final var presentation = renderPie(width, height);
        presentation.getSlides().forEach(slide -> {
            slide.getShapes().forEach(shape -> {
                if (shape instanceof IChart) {
                    final var chart = (IChart) shape;
                    chart.setLegend(true);
                }
            });
        });
        return presentation;
    }

    @Test
    public void testPyramidWithLabelsToPpt() {
        saveToFile(renderPyramidWithLabels(300, 300),
                SaveFormat.Ppt, "pyramidWithLabels.ppt");
    }

    @Test
    public void testPyramidWithLabelsToPptx() {
        saveToFile(renderPyramidWithLabels(300, 300),
                SaveFormat.Pptx, "pyramidWithLabels.pptx");
    }

    @Test
    public void testPieWithLabelsToPpt() {
        saveToFile(renderPieWithLabels(300, 300),
                SaveFormat.Ppt, "pieWithLabels.ppt");
    }

    @Test
    public void testPieWithLabelsToPptx() {
        saveToFile(renderPieWithLabels(300, 300),
                SaveFormat.Pptx, "pieWithLabels.pptx");
    }
}
